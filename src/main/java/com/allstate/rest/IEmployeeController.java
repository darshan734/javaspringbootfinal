package com.allstate.rest;

import com.allstate.entities.Employee;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface IEmployeeController {
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    String getStatus();

    @RequestMapping(value ="/total", method = RequestMethod.GET)
    long getTotal();

    @RequestMapping(value ="/all", method = RequestMethod.GET)
    List<Employee> all();

    @RequestMapping(value ="/find/{id}", method = RequestMethod.GET)
    Employee find(@PathVariable("id") int id);

    @RequestMapping(value ="/save", method = RequestMethod.POST)
    void save(@RequestBody Employee employee);

    @RequestMapping(value ="/update/{id}", method = RequestMethod.PUT)
    void update(@RequestBody Employee employee, @PathVariable("id") int id);
}
