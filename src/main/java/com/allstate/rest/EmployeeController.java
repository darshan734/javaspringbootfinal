package com.allstate.rest;

import com.allstate.entities.Employee;
import com.allstate.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class EmployeeController implements IEmployeeController {
    @Autowired
    private EmployeeService service;

    @Override
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus() {
        return "Rest Employee Api is running";
    }

    @Override
    @RequestMapping(value ="/total", method = RequestMethod.GET)
    public long getTotal() {
        return service.Total();
    }

    @Override
    @RequestMapping(value ="/all", method = RequestMethod.GET)
    public List<Employee> all() {
        return service.all();
    }

    @Override
    @RequestMapping(value ="/find/{id}", method = RequestMethod.GET)
    public Employee find(@PathVariable("id") int id) {
        return service.findById(id);
    }

    @Override
    @RequestMapping(value ="/save", method = RequestMethod.POST)
    public void save(@RequestBody Employee employee) {
         service.save(employee);
    }

    @Override
    @RequestMapping(value ="/update/{id}", method = RequestMethod.PUT)
    public void update(@RequestBody Employee employee, @PathVariable("id") int id) {
        employee.setId((id));
        service.update(employee);
    }
}
