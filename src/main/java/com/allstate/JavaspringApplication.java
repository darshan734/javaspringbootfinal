package com.allstate;

import com.allstate.di.Owner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class JavaspringApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(JavaspringApplication.class, args);
		context.getBean(Owner.class).getPet().feed();
	}

}
